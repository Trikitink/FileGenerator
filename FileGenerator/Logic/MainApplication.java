package Logic;
import UserInterface.*;
/*
 * Application for generating empty files
 * with chosen file name and extension
 * 
 * Made By
 * Kewin Bobel
 */
public class MainApplication {
	
	public static void main(String[] args) {
		
		new GraphicalInterface();
		
	}

}
