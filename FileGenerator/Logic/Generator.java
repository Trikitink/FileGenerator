package Logic;
import java.io.*;

//This class generates directions and empty files

public class Generator {

	private String filesDirection = "E:/Programowanie/Test/";

	File location;
	
	public Generator(String fName, String fExt, int amount){
				
		createNewDirection(filesDirection);
		
		createMultipleFiles(fName, fExt, filesDirection, amount);
		
	}
	
	//Method that creates chosen amount of files
	
	private void createMultipleFiles (String fileName, String fileExtension, 
									String fileLocation, int ammount){
		
		String fullFilePlace;
		
		try {
			
			for (int i = 1 ; i <= ammount ; ++i){
			
			fullFilePlace = fileLocation + fileName + "_" + i + fileExtension;	
			File random = new File(fullFilePlace);
			random.createNewFile();
			
			}
			
		} catch (IOException e) {

			e.printStackTrace();
		} 
		
	}
	
	//Method that creates chosen location
	
	private void createNewDirection(String newDirection){
		
		location = new File(newDirection);
		location.mkdirs();
		
	}
	
}
