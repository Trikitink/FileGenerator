package UserInterface;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import Logic.Generator;

import javax.swing.*;

public class GraphicalInterface extends JFrame{
	
	/**
	 * Main UI of application
	 */
	private static final long serialVersionUID = 1L;
	
	private Dimension sizeOfWindow;
	private Dimension locationOfWindow;

	//Toolkit to get size of screen, to locate frame at center
	private Toolkit tk = Toolkit.getDefaultToolkit();
	
	//Button for finalizing setting
	private JButton generateButton;
	
	//Main panel to set everything up
	private JPanel mainPanel;
	
	//Field for files name
	private JTextField fileNameField;
	
	//Box with extensions to choose
	private JComboBox<String> extensionsBox;
	private String[] extensions = {".jpg", ".png", ".mpg", ".mp3"};

	//Field for amount of files to generate
	private JFormattedTextField numberOfFiles;

	public GraphicalInterface(){
		
		int locX, locY;
		locationOfWindow = tk.getScreenSize();
		
		NumberFormat formatter = NumberFormat.getNumberInstance(Locale.getDefault());
		DecimalFormat decim = (DecimalFormat) formatter;
		
		sizeOfWindow = new Dimension(350, 100);
		generateButton = new JButton("Generate Files");
		mainPanel = new JPanel();
		fileNameField = new JTextField("File Name...", 15);
		numberOfFiles = new JFormattedTextField(decim);
		extensionsBox = new JComboBox<>(extensions);
		
		ListenForFieldClick lForClick = new ListenForFieldClick();
		ListenForButton lForButton = new ListenForButton();
		
		numberOfFiles.setColumns(3);
		fileNameField.addMouseListener(lForClick);
		numberOfFiles.addMouseListener(lForClick);
		generateButton.addActionListener(lForButton);
		
		locX = (int)(locationOfWindow.getWidth()/2 - sizeOfWindow.getWidth()/2);
		locY = (int)(locationOfWindow.getHeight()/2 - sizeOfWindow.getHeight()/2);
		
		this.setSize(sizeOfWindow);
		this.setVisible(true);
		this.setResizable(false);
		this.setLocation(locX, locY);
		this.setTitle("Random Files Generator");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		AddingRulesToPanel(mainPanel);
		
		this.add(mainPanel);
	}
	
	private void AddingRulesToPanel(JPanel mainPanel){
		
		GridBagConstraints layoutRules;
		
		mainPanel.setLayout(new GridBagLayout());
		layoutRules = new GridBagConstraints();
		layoutRules.insets = new Insets(5,5,5,5);
		layoutRules.anchor = GridBagConstraints.WEST;

		layoutRules.gridx = 1;
		layoutRules.gridy = 1;
		layoutRules.fill = GridBagConstraints.BOTH;
		mainPanel.add(fileNameField,layoutRules);

		layoutRules.fill = GridBagConstraints.NONE;
		layoutRules.gridx = 2;
		mainPanel.add(extensionsBox, layoutRules);
		layoutRules.gridy = 2;
		mainPanel.add(generateButton, layoutRules);
		layoutRules.gridx = 1;
		mainPanel.add(numberOfFiles,layoutRules);
		
	}
	
	private class ListenForFieldClick implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
		//	e.setText("");
			JTextField temp = (JTextField)e.getComponent();
			temp.setText("");
			
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class ListenForButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int temp = Integer.parseInt(numberOfFiles.getText());
			String extension = (String)extensionsBox.getSelectedItem();
			new Generator(fileNameField.getText(), extension, temp);
			
		}
		
	}
	
} 

























